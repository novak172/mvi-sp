# mvi-sp

### Zadání práce
Vezměte krátké 5-10 sekundové video ze sportovní hry dle vašeho výběru a zaměňte uniformu týmů. Případně vezměte krátké video s vlajkou a vyměňte jej za vlajku podle vlastního výběru.


---
Práci jsem vyvíjel v prostředí deepnote, níže naleznete odkaz na repozitář se všemy zdrojovými kódy, uloženými vahami a datasetem.

https://deepnote.com/project/Untitled-Python-Project-5zsPdeYUTQC2coa1CMJKJg/%2Fmvi-sem.ipynb


Vzhledem k tomu, že trénování neproběhlo v pořádku (popsáno v reportu), jsem vynechal generování videa s vlajkou. Nemělo by žádnou vypovídající hodnotu.
V "use.ipynb" naleznete alespoň kód který vezme obrázek, nahraje váhy modelu a obr. transformuje.
